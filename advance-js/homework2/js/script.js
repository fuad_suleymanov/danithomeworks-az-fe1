class Hamburger {
    flag = true;
    constructor(size, stuffing) {
        try {
            if (!size) throw 'no size given';
            if (size.type !== 'size') throw 'size should be small or large';
            this.size = size;

            if (!stuffing || stuffing.type !== 'stuffing') throw 'Stuffing argument is wrong';
            this.stuffing = [stuffing];

            this.toppings = [];

        } catch (e) {
            console.log(e)
        }
    }

    static SIZE_SMALL = {
        name: 'small',
        price: 50,
        calori: 20,
        type: 'size'
    };
    static SIZE_LARGE = {
        name: 'Large',
        price: 100,
        calori: 40,
        type: 'size'
    };
    static STUFFING_CHEESE = {
        name: 'Cheese',
        price: 10,
        calori: 20,
        type: 'stuffing'
    };
    static STUFFING_SALAD = {
        name: 'Salad',
        price: 20,
        calori: 5,
        type: 'stuffing'
    };
    static STUFFING_POTATO = {
        name: 'Potato',
        price: 15,
        calori: 10,
        type: 'stuffing'
    };
    static TOPPING_MAYONEZ = {
        name: 'Mayonez',
        price: 20,
        calori: 5,
        type: 'topping'
    };
    static TOPPING_SPICE = {
        name: 'Spice',
        price: 20,
        calori: 5,
        type: 'topping'
    };


    addTopping(topping) {
        try {
            if (!topping || topping.type !== 'topping') throw 'Not Valid Topping';
            if (this.toppings.find((item) => item.name === topping.name)) throw `duplicate topping ${topping.name}`
            this.toppings.push(topping);
        } catch (e) {
            console.log(e);
        }
    }

    removeTopping(topping) {
        try {
            if (!topping || topping.type !== 'topping') throw 'Not Valid Topping';
            const itemIndex = this.toppings.findIndex((item) => item.name === topping.name);
            if (itemIndex < 0) throw 'Topping Not exist';
            this.toppings.splice(itemIndex, 1);
        } catch (e) {
            console.log(e);
        }
    }

    getToppings() {
        return this.toppings;
    }

    getSize(){
        if(this.flag === true)return 'small';
        else return 'large';
    }

    getStuffing() {
        return this.stuffing;
    }

    calculatePrice() {
        return [this.size, ...this.stuffing, ...this.toppings].reduce((sum, item) => sum + item.price, 0);
    }

    calculateCalory() {
        return [this.size, ...this.stuffing, ...this.toppings].reduce((sum, item) => sum + item.calori, 0);
    }
}

// we already begin work

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYONEZ);
// спросим сколько там калорий
console.log(`Calories: ${hamburger.calculateCalory()}`);
// сколько стоит
console.log(`Price: ${hamburger.calculatePrice()}`);
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log(`Price: ${hamburger.calculatePrice()}`);
// Проверить, большой ли гамбургер?
console.log(hamburger.getSize());
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

// не передали обязательные параметры
// let h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
// let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);

// добавляем много добавок
// let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// h4.addTopping(Hamburger.TOPPING_MAYONEZ);
// h4.addTopping(Hamburger.TOPPING_MAYONEZ);
// HamburgerException: duplicate topping 'TOPPING_MAYO'