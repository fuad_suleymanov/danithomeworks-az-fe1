function Hamburger(size, stuffing) {
    try {
        if (!size) throw 'no size given';
        if (size.type !== 'size') throw 'Size argument is wrong';
        this.size = size;

        if (!stuffing || stuffing.type !== 'stuffing') throw 'Stuffing argument is wrong';
        this.stuffing = [stuffing];

        this.toppings = [];
    } catch (e) {
        alert(e);
    }
}

Hamburger.SIZE_SMALL = {
    name: 'Small',
    price: 50,
    calorie: 20,
    type: 'size'
};
Hamburger.SIZE_LARGE = {
    name: 'Large',
    price: 100,
    calorie: 40,
    type: 'size'
};
Hamburger.STUFFING_CHEESE = {
    name: 'Cheese',
    price: 10,
    calorie: 20,
    type: 'stuffing'
};
Hamburger.STUFFING_SALAD = {
    name: 'Salad',
    price: 20,
    calorie: 5,
    type: 'stuffing'
};
Hamburger.STUFFING_POTATO = {
    name: 'Potato',
    price: 15,
    calorie: 10,
    type: 'stuffing'
};
Hamburger.TOPPING_MAYONEZ = {
    name: 'Mayonez',
    price: 20,
    calorie: 5,
    type: 'topping'
};
Hamburger.TOPPING_SPICE = {
    name: 'Spice',
    price: 20,
    calorie: 5,
    type: 'topping'
};

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping || topping.type !== 'topping') throw 'Not Valid Topping';
        if (this.toppings.find((item) => item.name === topping.name)) throw `duplicate topping ${topping.name}`;
        this.toppings.push(topping);
    } catch (e) {
        alert(e);
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!topping || topping.type !== 'topping') throw 'Not Valid Topping';
        const itemIndex = this.toppings.findIndex((item) => item.name == topping.name);
        if (itemIndex < 0) throw 'Topping Not exist';
        this.toppings.splice(itemIndex, 1);
    } catch (e) {
        alert(e);
    }
};

Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

Hamburger.prototype.getSize = function () {
    return this.size;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    return [this.size, ...this.stuffing, ...this.toppings].reduce((sum, item) => sum + item.price, 0);
};

Hamburger.prototype.calculateCalories = function () {
    return [this.size, ...this.stuffing, ...this.toppings].reduce((sum, item) => sum + item.calorie, 0);
};

// we already begin work

/// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYONEZ);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


// не передали обязательные параметры
// let h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
// let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
// let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// h4.addTopping(Hamburger.TOPPING_MAYONEZ);
// h4.addTopping(Hamburger.TOPPING_MAYONEZ);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
