//swiper
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    loop: true,
    freeMode: true,
    loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    loop: true,
    loopedSlides: 5, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});


// TAB regulation

// I get tab buttons
let allTabBtn = document.querySelector('#tab');
// console.log(allBtn);

allTabBtn.addEventListener('click',
    function (e) {
        let target = e.target;
        // below we change target tab style which we click
        target.style.backgroundColor = '#18cfab';
        target.style.color = '#f8fcfe';

        // we get atttibute value from target element
        let attrTitle = target.getAttribute('data-action');

        // we get texts for titles
        let textLi = document.querySelectorAll('.text');
        console.log(textLi);

        // in this loop we show text which we click and hide others
        for (let litext of textLi) {
            if (litext.getAttribute('data-text') == attrTitle) {
                litext.style.display = 'block';
            } else {
                litext.style.display = 'none';
            }
        }

        //we get all li elements
        let liEls = document.querySelectorAll('.tabs-title');

        // in this loop we change all no target title style
        for (let li of liEls) {
            if (li !== target) {
                li.style.backgroundColor = '#f8fcfe';
                li.style.color = 'rgba(113, 113, 113, 255)';
            }
        }
    });


// load tab below

const loadedData = [
    {
        imgSrc: 'img/tab-img1.jpg',
        class: 'img-tab-load',
        hoverText: 'Hover',
        category: 1,
        tabName: 'Graphic Design'
    },
    {
        imgSrc: 'img/tab-img2.jpg',
        hoverText: 'Hover',
        category: 2,
        tabName: 'Web Design'
    },
    {
        imgSrc: 'img/tab-img3.jpg',
        hoverText: 'Hover',
        category: 3,
        tabName: 'Landing Pages'
    },
    {
        imgSrc: 'img/tab-img4.jpg',
        hoverText: 'Hover',
        category: 4,
        tabName: 'WordPress'
    },
    {
        imgSrc: 'img/tab-img1.jpg',
        hoverText: 'Hover',
        category: 1,
        tabName: 'Graphic Design'
    },
    {
        imgSrc: 'img/tab-img2.jpg',
        hoverText: 'Hover',
        category: 2,
        tabName: 'Web Design'
    },
    {
        imgSrc: 'img/tab-img3.jpg',
        hoverText: 'Hover',
        category: 3,
        tabName: 'Landing Pages'
    },
    {
        imgSrc: 'img/tab-img4.jpg',
        hoverText: 'Hover',
        category: 4,
        tabName: 'WordPress'
    },
    {
        imgSrc: 'img/tab-img1.jpg',
        hoverText: 'Hover',
        category: 1,
        tabName: 'Graphic Design'
    },
    {
        imgSrc: 'img/tab-img2.jpg',
        hoverText: 'Hover',
        category: 2,
        tabName: 'Web Design'
    },
    {
        imgSrc: 'img/tab-img3.jpg',
        hoverText: 'Hover',
        category: 3,
        tabName: 'Landing Pages'
    },
    {
        imgSrc: 'img/tab-img4.jpg',
        hoverText: 'Hover',
        category: 4,
        tabName: 'WordPress'
    }
];

// Prepare func for load more logic
const loadMore = () => {
    let items = [];
    console.log(`loadMOre BasilanKimi: ${$('.img-tab-load').length}`);
    // Create out new items
    for (let item of loadedData) {
        items.push(`
                    <a href="#" class="project-item category-${item.category}">
                        <img class="img-tab-load" src="${item.imgSrc}" alt="">
                        <div class="project-item--hover">
                        <div class="hover">
                                    <div class="circles">
                                        <div class="epmty-circle"><img src="img/empty-circle.png" alt=""></div>
                                        <div class="full-circle"><img src="img/full-circle.png" alt=""></div>
                                        <div class="search-hover"><img src="img/search-white.png" alt=""></div>
                                        <div class="in-empty-hover"><img src="img/in-empty-circle.png" alt=""></div>
                                    </div>
                                    <div class="crea-design">CREATIVE DESIGN</div>
                                    <div class="tab-name">${item.tabName}</div>
                                </div>
                        </div>
                    </a>
                `);

    }


    // Find containers
    const wrapper = $('.grid-wrapper');
    const inner = $('.grid');

    // Fix current height
    wrapper.css('height', wrapper.height());
    // Add new items
    inner.append(items.join(''));
    // Animate to new height
    wrapper.animate({
        height: inner.height()
    }, 100, () => {
        // Restore auto height after animation
        wrapper.css('height', 'auto')
    });

    let $imgLen =  $('.img-tab-load').length;
    console.log($imgLen);
    if($imgLen > 12) {
        $('.load-more').hide();
    }
};

let $imgLen =  $('.img-tab-load').length;


$('.load-more').on('click', loadMore);

$('.categories').on('click', '.category-item', function (e) {
    if (!$(this).hasClass('active')) {
        $('.load-more').hide();
        const wrapper = $('.grid-wrapper');
        const inner = $('.grid');
        wrapper.css('height', wrapper.height());
        $('.loader').fadeIn(100, () => {
            $('.category-item.active').removeClass('active');
            $(this).addClass('active');
            // $(this).addClass('img-tab-load');

            const categoryNum = $(this).attr('data-category');


            if (categoryNum == '0') {
                $('.project-item').show();
                let $imgLen1 = $('.img-tab-load').length;
                console.log($imgLen1);
                if($imgLen1>12){
                    $('.load-more').hide();
                }else{
                    $('.load-more').show();
                }
            } else {
                $('.project-item').hide();
                $('.project-item.category-' + categoryNum).show();
            }

            wrapper.animate({
                height: inner.height()
            }, 600, () => {
                // Restore auto height after animation
                wrapper.css('height', 'auto');
                $('.loader').fadeOut(100)
            })
        })
    }else if($(this).hasClass('active')){
        $('.load-more').show();
    }
    // $(this).addClass('img-tab-load');
    // countLoadImages = $('.img-tab-load').length;
    // console.log(`after loading: ${countLoadImages}`);


});

$lenHover = $('div:contains("hover")').length;
console.log(`hover div items: ${$lenHover}`);

