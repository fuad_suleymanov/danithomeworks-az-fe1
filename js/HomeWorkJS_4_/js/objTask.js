/*
Created by Fuad Suleymanov
Creatin date: 26.10.2019
Revised_2 date: 31.10.2019
 */

function creatNewUser(name, lastname) {
    let obj = {
        firstName: name,
        lastName: lastname,
        getLogin: function () {
            return obj.firstName.charAt(0).toLowerCase() + obj.lastName.toLowerCase();
        }
    };
    return obj;
}


let name = prompt("Enter you first name: ","");
let lastname = prompt("Enter you last name: ","");
// creat object with function
let newObj = creatNewUser(name, lastname);

// here I call get login funqtion and display result in consol
console.log(newObj.getLogin());

//print current newUser
console.log(newObj);

//add setter for change firstName property in newUser object
Object.defineProperty(newObj, 'setFirstName', {set: function (newValue) {this.firstName = newValue;} });

//add setter for change lastName property in newUser object
Object.defineProperty(newObj, 'setLastName', {set: function (newValue) {this.lastName = newValue;} });

// change firstName property with setter
newObj.setFirstName = prompt("Add new First Name with setter: ");

// change lastName property with setter
newObj.setLastName = prompt("Add new Last Name with setter: ");

// after changes with setters
console.log(newObj);
