/*
Creat by: Fuad Suleymanov
Creation date: 09.11.2019
 */


// get input element
let inp = document.querySelector('.inpPrice');

// get lits(ul) element
let listItem = document.querySelector('.list');

// when focus on input field
inp.addEventListener("focus", onFocusing);

// when focus out on input field
inp.addEventListener("focusout", onNonFocusing);

// this event delete prices(list items) with x icon
listItem.addEventListener('click', removeLi);

// focus out function, our main function
function onNonFocusing() {
    // teke value from input field
    let priceValue = inp.value;
    // this if give us: show only correct price
    if (!isNaN(priceValue) && priceValue >= 0 && priceValue != "" && priceValue != null){
        // change input field border color to black
        this.style.border = '3px solid #080404';
        // add li element
        let liEl = document.createElement('li');
        // add class to li element
        liEl.className = 'currentP';
        // add text(price) to li element
        liEl.appendChild(document.createTextNode(`Current Price: ${priceValue}`));
        // creat deleteBtn button
        let deleteBtn = document.createElement('button');
        // add current classes to deleteBtn
        deleteBtn.className = "delete-icon";
        // add X to deleteBtn
        deleteBtn.appendChild(document.createTextNode('x'));
        // add deleteBtn to li
        liEl.appendChild(deleteBtn);
        // take parent element of list
        let ulEl = document.querySelector('.list');
        // we check if we have childnode, we delete he!
        if (listItem.hasChildNodes()){
            let li = document.querySelector('li');
            listItem.removeChild(li);
        }
        // add ready li to ul
        ulEl.appendChild(liEl);

        // we get no correct prices
    } else if (isNaN(priceValue) || priceValue < 0){
        // when we get no correct price change input frame to red
        this.style.border = '3px solid #FF2012';
        // add li element
        let liEl = document.createElement('li');
        // add class to li element
        liEl.className = 'error';
        // add text(price) to li element
        liEl.appendChild(document.createTextNode(`Please enter correct price`));
        // we check if we have childnode, we delete he!
        if (listItem.hasChildNodes()){
            let li = document.querySelector('li');
            listItem.removeChild(li);
        }
        // add ready li to ul
        listItem.appendChild(liEl);
    } else {
        // change input field border color to black
        this.style.border = '3px solid #080404';
    }

    // delete value from input field
    inp.value = '';
}

// here we only change input fild area border color to green
function onFocusing() {
    this.style.border = '3px solid #6CE807';
}

// with this function we remove price when click to x
function removeLi(e){
    // with this we remove when we click only on x
    if(e.target.classList.contains('delete-icon')){
        // take list
        let li = e.target.parentElement;
        // remove
        listItem.removeChild(li);
    }
}
