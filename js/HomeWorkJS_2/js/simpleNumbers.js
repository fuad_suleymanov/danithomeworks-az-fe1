/*
Created by Fuad Suleymanov
creation date: 26.10.2019
HomeWork 2 (Simple number - optional)
 */

let number1m = 0;
let number2n = 0;

let numbers = prompt("Please enter two number (sepatereted with one space): ", "");

// in this while I do validation for NaN and for second number must be higher than first
while (true) {

    number1m = +numbers.split(" ")[0];
    number2n = +numbers.split(" ")[1];

    if (isNaN(number1m) || isNaN(number2n)) {
        numbers = prompt("Wrong input!!! Please enter numbers again (sepatereted with one space): ")
        continue;
    }

    if (number1m < number2n) {
        break;
    } else {
        numbers = prompt("Wrong input!!! first number must be higther than second, enter again (sepatereted with one space): ")
    }
}

console.log("You entered two numbers: " + number1m + ", " + number2n + ". In below you can see simple numbers in range your numbers:" );

// in this for I find prime number in range of my number and print then to consol
for (let i = number1m; i <= number2n; i++) {
    let count = 0;
    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            count++;
        }
    }
    if (count == 0) {
        console.log(i)
    }
}

