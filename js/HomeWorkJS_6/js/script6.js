/*
Creat by Fuad Suleymanov
creation date: 31.10.19
 */

// this function return us filted array (by type)
function filterBy(arr, varibleType) {
    let filteredArr = [];
    arr.forEach(function (elment) {
        if(typeof elment != varibleType){
            filteredArr.push(elment);
        }
    });
    return filteredArr;
}

// excample array
let arr = ['hello', 'world', 23, '23', null, true];

// here we call above function and print result to console
let filteredArr = filterBy(arr, "string");
console.log(filteredArr);


