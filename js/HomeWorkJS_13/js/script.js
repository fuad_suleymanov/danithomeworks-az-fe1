// we creat link element
let head = document.head;
link = document.createElement('link');
link.rel = 'stylesheet';

// check localstorage value
if (localStorage.getItem('themeStyle') === 'dark') {
    link.href = 'css/dark.css'; // ссылка на темный стиль
    // document.getElementById('switch-1').setAttribute('checked', true); // переключаем чекбокс в положение "темная тема"
}
// default is light mode
else {
    link.href = 'css/main5.css'; // default light style
}

head.appendChild(link); // add created link element to head of file

// event listener with delegation
document.querySelector('.swich-mode').addEventListener('click', e => {
    let btn = e.target;
    //  if we click to dark-mode
    if (btn.textContent == 'Dark_Style') {
        link.href = 'css/dark.css'; // change style to dark
        localStorage.setItem('themeStyle', 'dark'); // write value to localStorage
    }
    else {
        link.href = 'css/main5.css'; // change to light mode
        localStorage.setItem('themeStyle', 'light'); // write value to localStorage
    }
});
