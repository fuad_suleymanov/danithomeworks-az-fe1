/*
Createt by Fuad Suleymanov
creation date: 31.10.2019
Home_Work_JS_5
 */

// with this function we creat user object
function creatNewUser(name, lastname, birthday) {
    let obj = {
            firstName: name,
            lastName: lastname,
            birthday: birthday,
            getAge: function () {
                let arr = birthday.split('.');
                let parseToDate = new Date(arr[2], arr[1] - 1, arr[0]);
                let getBirthYear = parseToDate.getFullYear();
                let getBirthMonth = parseToDate.getMonth() + 1;
                let getBirthDay = parseToDate.getDate();
                let currentDate = new Date();
                let currentYear = currentDate.getFullYear();
                let currentMonth = currentDate.getMonth() + 1;
                let currentDay = currentDate.getDate();
                if (getBirthMonth < currentMonth || getBirthMonth == currentMonth && getBirthDay <= currentDay) {
                    return currentYear - getBirthYear;
                } else {
                    return currentYear - getBirthYear - 1;
                }
            },
            getPassword: function () {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2];
            }
        }
    ;
    return obj;
}

let name = prompt("Enter you first name: ", "");
let lastname = prompt("Enter you last name: ", "");
let birthday = prompt("add you birthday (this format 'dd.mm.yyyy'): ");

// creat object with function
let newObj = creatNewUser(name, lastname, birthday);

// here I call getAge method and print it to consol
console.log(`${newObj.firstName}'s age is ${newObj.getAge()}`);

// here I call getPassword method and print is to consol
console.log(`Your best secure password: ${newObj.getPassword()}`);

//print current newUser
console.log(newObj);




