
// get buttons
let btns = document.querySelectorAll('.btn');

// our event listener for keyboard
document.addEventListener('keydown', doBlue);

/* first I get target(button which pressed), then check with switch if it is one of
the - Enter, S, E, O, N, L, Z - then change this button color to blue,
and finally I loop and change all no target buttons color to black
 */
function doBlue(e) {
    let target = e.code;
    console.log(target);
    switch (target) {
        case 'Enter':
            btns[0].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[0] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyS':
            btns[1].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[1] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyE':
            btns[2].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[2] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyO':
            btns[3].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[3] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyN':
            btns[4].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[4] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyL':
            btns[5].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[5] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
        case 'KeyZ':
            btns[6].style.backgroundColor = 'blue';
            for (let btn of btns) {
                if (btns[6] == btn) {
                    continue;
                }
                btn.style.backgroundColor = '#33333a';
            }
            break;
    }
}