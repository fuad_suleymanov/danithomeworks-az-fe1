

// get our input elements
let pwds = document.querySelectorAll('.pwd');

// get our eye elements
let eyes = document.querySelectorAll('.fas');

// get confirm button
let btn = document.querySelector('.btn');

// this event listeners show us passwor whwn we click on icon
eyes[0].addEventListener('click', showPassword);
eyes[1].addEventListener('click', showPassword1);

// this events listeners remove error text we again focus on input field
pwds[0].addEventListener('click', onFocus);
pwds[1].addEventListener('click', onFocus);

// this event compare password and say Welcome to user or show error text
btn.addEventListener('click', doConfirm);



function doConfirm(){
    let pwdValue1 = pwds[0].value;
    let pwdValue2 = pwds[1].value;
    if(pwdValue1 == pwdValue2){
        alert('You are welcome');
    } else{
        let errorEl = document.createElement("p");
        errorEl.className = 'error';
        errorEl.appendChild(document.createTextNode('You must enter same passwords'));
        let erEl = document.querySelector('.error');
        if(!erEl.hasChildNodes()){
            erEl.appendChild(errorEl);
        }
    }
}

// this function for first input field
function showPassword(e) {
    let parent = e.target.parentElement;
    console.log(parent);
    // with this we remove when we click only on x
    e.target.classList.toggle('fa-eye-slash');
    // change type attribute value
    pwds[0].type == 'password' ? pwds[0].type = 'text' : pwds[0].type = 'password';
    // console.log(e.target.getAttribute('type'));
    // console.log(e.target);
}

// this function for second input field
function showPassword1(e) {
    let parent = e.target.parentElement;
    console.log(parent);
    // with this we remove when we click only on x
    e.target.classList.toggle('fa-eye-slash');
    // change type attribute value
    pwds[1].type == 'password' ? pwds[1].type = 'text' : pwds[1].type = 'password';
    // console.log(e.target.getAttribute('type'));
    // console.log(e.target);
}


function onFocus() {
    let erEl = document.querySelector('.error');
    if (erEl.hasChildNodes()){
        let element = document.querySelector('p')
        element.remove();
    }
}
