/*
Created by Fuad Suleymanov
Creatin date: 26.10.2019
Revised_2 date: 31.10.2019
 */

//below is my function
function calculator(num1, num2, operator) {

    // below I check user's numbers
    let status = true;
    while (status) {
        if (isNaN(num1) || isNaN(num2)) {
            numbers = prompt("Wrong input!!! Enter two numbers (sepatereted with one space): ", "");
            num1 = +numbers.split(" ")[0];
            num2 = +numbers.split(" ")[1];
        } else {
            status = false;
        }
    }

    //below i check input for operator
    status = true;
    while (status) {
        if (operator === '-' || operator === '+' || operator === '*' || operator === '/') {
            status = false;
        } else {
            operator = prompt("Wrong operator!!! Enter one of the this math operators: '-' '+' '*' '/' : ", "");
        }
    }

    // below calculation
    let equal = 0;
    switch (operator) {
        case '-':
            equal = num1 - num2;
            break;
        case '+':
            equal = num1 + num2;
            break;
        case '*':
            equal = num1 * num2;
            break;
        default:
            equal = "division to zero is forbidden!!!";
    }
    console.log(`${num1} ${operator} ${num2} = ${equal}`);
}

// below I enter number and operator for calculation
let numbers = prompt("Enter two numbers (sepatereted with one space): ", "");
let num1 = +numbers.split(" ")[0];
let num2 = +numbers.split(" ")[1];
let operator = prompt("Enter one of the this math operators: '-' '+' '*' '/' : ", "");

// below I call function and get result in consol!
calculator(num1, num2, operator);



