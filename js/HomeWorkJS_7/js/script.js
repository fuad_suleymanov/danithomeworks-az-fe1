
// this is main function, creat all html and show list in page
function creatList(arr){
    let div = document.createElement("div");
    document.body.appendChild(div);

    let ul = document.createElement('ul');
    div.appendChild(ul);

    let newArray = arr.map(myfunction);

    function myfunction(el) {
        let li = document.createElement('li');
        ul.appendChild(li);
        li.innerHTML = el;
    }
}

// excample array
let arr = ['hello', 'world', 'Baku', 'IBA Tech Academy', '2019'];

// call my function
creatList(arr);

// below creat countdown and clean page
let timer = 10;
let downloadTimer = setInterval(function(){
    let div1 = document.createElement('div');
    div1.className = 'container';
    document.body.appendChild(div1);
    document.querySelector(".container").innerHTML = `<h1>${timer} seconds...</h1>`;
    timer -= 1;
    if(timer <= -1){
        clearInterval(downloadTimer);
        document.querySelector("body").style.display = "none";
    }
}, 1000);


