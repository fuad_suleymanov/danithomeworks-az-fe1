/*
Created by Fuad Suleymanov
creation date: 25.10.2019
revised_1 date: 29.10.2019
HomeWork 1
 */

let userName = prompt("Please enter your name: ", " ");

// this function check it is name or not
function isName(str) {
    for (let i = 0; i < str.length; i++) {
        if (str === " " || str.length === 0 || str[i].toLowerCase() === str[i].toUpperCase()) {
            return false;
        }
    }
    return true;
}

// this variables for break loop and enter other loop
let changeToTry = 0;
let youDoneItName = false;

while (changeToTry < 3) {
    if (!isName(userName)) {
        userName = prompt("Wrong name!!! Please enter your name again: ", " ");
        changeToTry++;
    } else {
        youDoneItName = true;
        break;
    }
}

if (!youDoneItName) {
    alert("Today is not you day!!! You can't enter you own name! Tomorrow try again!")
}

changeToTry = 0;
youDoneItAge = false;

let userAge;
if (youDoneItName) {
    userAge = prompt("Please enter your age: ", " ");
}

// this scope for validation age
if (youDoneItName) {
    while (changeToTry < 3) {
        if (!isNaN(userAge) && userAge !== " ") {
            youDoneItAge = true;
            break;
        } else {
            userAge = prompt("Wrong age!!! Please enter your age again: " , " ");
            changeToTry++;
        }
    }
}

if (youDoneItName && !youDoneItAge) {
    alert("Today is not you day!!! You can't enter you own age! Tomorrow try again!")
}

if (youDoneItAge) {
    if (userAge < 18) {
        alert("You are not allow to visit website.")
    } else if (userAge >= 18 && userAge <= 22) {
        let check = confirm("Are you sure you want to continue?");
        if (check) {
            alert("Welcome " + userName);
        } else {
            alert("You are not allow to visit website.")
        }
    } else {
        alert("Welcome " + userName);
    }
}


